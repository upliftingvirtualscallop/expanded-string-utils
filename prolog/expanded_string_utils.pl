:- module(expanded_string_utils, [string_concat_with_space/3]).

% Copyright 2018-2019 ZombieChicken

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.

% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

string_concat_with_space(A,B,C) :-
    string_concat(A," ",X),
    string_concat(B," ",Y),
    string_concat(X,Y,C).
